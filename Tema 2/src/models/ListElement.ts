import mongoose, { Schema, Document } from "mongoose";

export interface IListElement extends Document {
  title: string;
  body: string;
  done: boolean;
}

export const ListElementSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  body: {
    type: String
  },
  // Cerinta 1 Tema 2: Model Understanding
  done: {
    type: Boolean
  }
});

export default mongoose.model<IListElement>("ListElement", ListElementSchema);
