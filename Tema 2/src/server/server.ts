import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import DriverManager from "../services/DriverManager";

export default class Server {
  private port: number;
  private app: express.Application;

  public constructor(app: express.Application, port: number) {
    this.port = port;
    this.app = app;

    this.configApp();
    this.setRoutes();
    this.startServer();

    DriverManager.Instance.connect();
  }
  private startServer() {
    this.app.listen(this.port, () => {
      console.log(`Server started at http://localhost:${this.port}!`);
    });
  }
  private configApp() {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));

    this.app.use(cors());
  }

  private setRoutes() {
    this.app.get(
      "/",
      (request: express.Request, response: express.Response) => {
        response.send("Hello word");
      }
    );

    this.app.get(
      "/lists",
      async (req: express.Request, res: express.Response) => {
        try {
          let lists = await DriverManager.Instance.getAllToDoLists();
          if (lists.length > 0) {
            res.send(lists);
          } else res.send("No List found");
        } catch {
          res.send("error");
        }
      }
    );
    this.app.get(
      "lists/:name",
      async (req: express.Request, res: express.Response) => {
        let nameRequested = req.params.name;

        try {
          let requestedList = await DriverManager.Instance.getListByName(
            nameRequested
          );
          res.send(requestedList);
        } catch {
          res.send("error");
        }
      }
    );
    this.app.post(
      "/addList",
      async (req: express.Request, res: express.Response) => {
        try {
          await DriverManager.Instance.addNewList(req.body.name);
          res.send("Success!");
        } catch {
          res.send("Error!");
        }
      }
    );
    this.app.post(
      "/addListElement",
      async (req: express.Request, res: express.Response) => {
        try {
          await DriverManager.Instance.addNewElementForList(
            req.body.listName,
            req.body.taskName,
            req.body.description
          );

          res.send("Success!");
        } catch {
          res.send("Error!");
        }
      }
    );

    // Cerinta 3: get ce returneaza un element al listei
    this.app.get(
      "/listElement/:name/:title",
      async (req: express.Request, res: express.Response) => {
        let nameRequested = req.params.name;
        let titleRequested = req.params.title;
        try {
          let requestedList = await DriverManager.Instance.getListByName(
            nameRequested
          );
          if (requestedList) {
            res.send(
              requestedList.listElements.find(el => el.title == titleRequested)
            );
          } else {
            res.send("No List found");
          }
        } catch {
          res.send("Error");
        }
      }
    );
    //Cerinta 4: Sterge un element list
    this.app.delete(
      "/listElement/:name/:title",
      async (req: express.Request, res: express.Response) => {
        let nameRequested = req.params.name;
        let titleRequested = req.params.title;
        try {
          await DriverManager.Instance.deleteListElement(
            nameRequested,
            titleRequested
          );
          res.send("succes");
        } catch {
          res.send("Error to delte list");
        }
      }
    );
    //Cerinta 5: stergere o intreaga lista
    this.app.delete(
      "/list/:name",
      async (req: express.Request, res: express.Response) => {
        let nameRequested = req.params.name;
        try {
          await DriverManager.Instance.deleteList(nameRequested);
          res.send("Succes");
        } catch {
          res.send("Error");
        }
      }
    );
  }
}
