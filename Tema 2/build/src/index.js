"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server_1 = __importDefault(require("./server/server"));
const app = express_1.default();
const port = 8080;
new server_1.default(app, port);
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended:false}));
// app.get("/", (req,res)=>{
//     res.send("You are here");
// });
// app.get("/nume",(req,res)=>{
//     res.send("nume");
// });
// app.get("/nume/:name",(req,res)=>{
//     res.send(`Welcome ${req.params.name}!`);
// });
// app.get("/lists",(req,res)=>{
//     res.send(mokedData.lists);
// });
// app.get("/lists/:id",(req,res)=>{
//     let idRequested=req.params.id;
//     let requestedList=mokedData.lists.filter(el=>el.id=== idRequested);
//     if(requestedList)
//     {
//         res.send(requestedList);
//     }
//     else {
//     res.send("No list found");}
// });
// app.get("/listElements",(req,res)=>{
//     res.send(mokedData.lists.filter(el=>el.listElements));
// });
// app.get("/title/:id/:id2",(req,res)=>{
//     let idRequested=req.params.id;
//     let listIdRequested=req.params.id2;
//     let requestedList=mokedData.lists.filter(el=>el.id==idRequested);
//     let requestedListWanted=requestedList[0].listElements.filter(el=> el.id==listIdRequested);
//    // res.send({title: requestedList[0].listElements[listIdRequested-1].title, body: requestedList[0].listElements[listIdRequested-1].body});
//    if(requestedListWanted){
//     res.send({title: requestedListWanted[0].title, body:requestedListWanted[0].body});
//    }
//    else{
//        res.send("no list found")
//    }
// });
// app.delete("/del/:id/:id2",(req,res)=>{
//     let idList=req.params.id;
//     let idElement=req.params.id2;
//     let requestedList=mokedData.lists.filter(el=>el.id===idList);
//     console.log("File to delete");
//     delete requestedList[0].listElements[idElement-1];
//     var fs= require('fs');
//     fs.writeFile("./Resources/MockedListData.json",JSON.stringify(mokedData), (err: any) => {
//         if (err) throw err;
//         res.send('Data deleted');
//     });
//     console.log("File deleted");
// });
// app.post("/actual/:id",(req,res)=>{
//     let idList=req.params.id;
//     if(req.body){
//         let reqId=req.body.id;
//         let reqTitle=req.body.title;
//         let reqDescription=req.body.description
//         console.log(reqId + " " +reqTitle + " "+ reqDescription);
//         var obj={"id": reqId, "title": reqTitle, "body": reqDescription};
//         mokedData.lists[idList-1].listElements.push(obj);
//         var fs= require('fs');
//         fs.writeFile("./Resources/MockedListData.json",JSON.stringify(mokedData), (err: any) => {
//             if (err) throw err;
//             console.log('Data written to file');
//             res.send('Data written to file');
//         });
//     }
// });
// app.listen(port, () => {
// 	console.log(`Server started at http://localhost:${port}!`);
// });
//# sourceMappingURL=index.js.map