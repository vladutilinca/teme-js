"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const DriverManager_1 = __importDefault(require("../services/DriverManager"));
class Server {
    constructor(app, port) {
        this.port = port;
        this.app = app;
        this.configApp();
        this.setRoutes();
        this.startServer();
        DriverManager_1.default.Instance.connect();
    }
    startServer() {
        this.app.listen(this.port, () => {
            console.log(`Server started at http://localhost:${this.port}!`);
        });
    }
    configApp() {
        this.app.use(body_parser_1.default.json());
        this.app.use(body_parser_1.default.urlencoded({ extended: false }));
        this.app.use(cors_1.default());
    }
    setRoutes() {
        this.app.get("/", (request, response) => {
            response.send("Hello word");
        });
        this.app.get("/lists", async (req, res) => {
            try {
                let lists = await DriverManager_1.default.Instance.getAllToDoLists();
                if (lists.length > 0) {
                    res.send(lists);
                }
                else
                    res.send("No List found");
            }
            catch {
                res.send("error");
            }
        });
        this.app.get("lists/:name", async (req, res) => {
            let nameRequested = req.params.name;
            try {
                let requestedList = await DriverManager_1.default.Instance.getListByName(nameRequested);
                res.send(requestedList);
            }
            catch {
                res.send("error");
            }
        });
        this.app.post("/addList", async (req, res) => {
            try {
                await DriverManager_1.default.Instance.addNewList(req.body.name);
                res.send("Success!");
            }
            catch {
                res.send("Error!");
            }
        });
        this.app.post("/addListElement", async (req, res) => {
            try {
                await DriverManager_1.default.Instance.addNewElementForList(req.body.listName, req.body.taskName, req.body.description);
                res.send("Success!");
            }
            catch {
                res.send("Error!");
            }
        });
        // Cerinta 3: get ce returneaza un element al listei
        this.app.get("/listElement/:name/:title", async (req, res) => {
            let nameRequested = req.params.name;
            let titleRequested = req.params.title;
            try {
                let requestedList = await DriverManager_1.default.Instance.getListByName(nameRequested);
                if (requestedList) {
                    res.send(requestedList.listElements.find(el => el.title == titleRequested));
                }
                else {
                    res.send("No List found");
                }
            }
            catch {
                res.send("Error");
            }
        });
        //Cerinta 4: Sterge un element list
        this.app.delete("/listElement/:name/:title", async (req, res) => {
            let nameRequested = req.params.name;
            let titleRequested = req.params.title;
            try {
                await DriverManager_1.default.Instance.deleteListElement(nameRequested, titleRequested);
                res.send("succes");
            }
            catch {
                res.send("Error to delte list");
            }
        });
        //Cerinta 5: stergere o intreaga lista
        this.app.delete("/list/:name", async (req, res) => {
            let nameRequested = req.params.name;
            try {
                await DriverManager_1.default.Instance.deleteList(nameRequested);
                res.send("Succes");
            }
            catch {
                res.send("Error");
            }
        });
    }
}
exports.default = Server;
//# sourceMappingURL=server.js.map