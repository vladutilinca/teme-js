"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const ToDoList_1 = __importDefault(require("../models/ToDoList"));
const ListElement_1 = __importDefault(require("../models/ListElement"));
class DriverManager {
    constructor() {
        this.MONGO_STRING = "mongodb://localhost:27017/ToDoListManager";
    }
    async connect() {
        mongoose_1.default.connect(this.MONGO_STRING, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useCreateIndex: true
        });
        let db = mongoose_1.default.connection;
        db.once("open", () => {
            console.log("Connected to " + this.MONGO_STRING);
        });
    }
    static get Instance() {
        if (this._instance) {
            return this._instance;
        }
        else {
            this._instance = new DriverManager();
            return this._instance;
        }
    }
    async addNewList(listName) {
        let newList = new ToDoList_1.default({
            name: listName
        });
        return await newList.save();
    }
    async getListByName(listName) {
        return await ToDoList_1.default.findOne({ name: listName }).exec();
    }
    async getAllToDoLists() {
        return await ToDoList_1.default.find({}).exec();
    }
    async addNewElementForList(listName, taskName, taskDescription) {
        let newItem = new ListElement_1.default({
            title: taskName,
            body: taskDescription
        });
        return await ToDoList_1.default.findOneAndUpdate({ name: listName }, { $push: { listElements: newItem } }).exec();
    }
    //Cerinta 4: Stergere ListElement
    async deleteListElement(listName, titleName) {
        let newItem = new ListElement_1.default({
            title: titleName
        });
        return await ToDoList_1.default.findOneAndUpdate({ name: listName }, { $pull: { listElements: { title: titleName } } }).exec();
    }
    //Cerinta 5: stergerea unei intregi liste
    async deleteList(listName) {
        return await ToDoList_1.default.findOneAndDelete({ name: listName }).exec();
    }
}
exports.default = DriverManager;
//# sourceMappingURL=DriverManager.js.map