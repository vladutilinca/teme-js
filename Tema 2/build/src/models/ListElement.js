"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importStar(require("mongoose"));
exports.ListElementSchema = new mongoose_1.Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String
    },
    // Cerinta 1 Tema 2: Model Understanding
    done: {
        type: Boolean
    }
});
exports.default = mongoose_1.default.model("ListElement", exports.ListElementSchema);
//# sourceMappingURL=ListElement.js.map