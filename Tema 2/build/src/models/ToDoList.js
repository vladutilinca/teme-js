"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ListElement_1 = require("./ListElement");
const mongoose_1 = __importStar(require("mongoose"));
const ToDoListSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        index: true
    },
    listElements: {
        type: [ListElement_1.ListElementSchema]
    }
}, { collection: "ToDoList" });
exports.default = mongoose_1.default.model("ToDoList", ToDoListSchema);
//# sourceMappingURL=ToDoList.js.map